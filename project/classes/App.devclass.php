<?php
/**
 * This file should only ever be loaded for local development.
 * On a production or staging system, the real "App" class from EOV should be loaded instead.
 * (This is handled automatically)
 */

/**
 * This class is a cut down version of the App class in the EOV core with the core
 * required functions for the landing page to function as a standalone application
 */
class App
{
    private static $data = null;
    private static $coreIniPath = "config/base.ini";
    private static $localDevIniPath = "config/localdev.ini";

    public static function init()
    {
        $coreSettings = self::parseIniFile(self::$coreIniPath);
        $localSettings = self::parseIniFile(self::$localDevIniPath);

        // Combine the core settings with the local settings
        $config = array_merge($coreSettings, $localSettings);

        self::$data = $config;
    }

    public static function getSetting($var = null, $default = null)
    {
        if ($var === null) {
            return self::$data;
        }

        return isset(self::$data[$var]) ? self::$data[$var] : $default;
    }

    private static function parseIniFile($path, $processSections = false)
    {
        $ini = @parse_ini_file($path, $processSections);

        if ($ini === false) {
            error_log("Failed to load $path");

            return array();
        }

        return $ini;
    }
}

App::init();
